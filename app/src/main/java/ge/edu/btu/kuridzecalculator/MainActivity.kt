package ge.edu.btu.kuridzecalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var firstNumber: Double = 0.0
    private var secondNumber: Double = 0.0
    private var operation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        buttonEqual.setOnClickListener {
            equal()
        }
        buttonZero.setOnClickListener {
            numberClick(buttonZero)
        }
        buttonSeven.setOnClickListener {
            numberClick(buttonSeven)
        }
        buttonEight.setOnClickListener {
            numberClick(buttonEight)
        }
        buttonNine.setOnClickListener {
            numberClick(buttonNine)
        }

        buttonSix.setOnClickListener {
            numberClick(buttonSix)
        }
        buttonFive.setOnClickListener {
            numberClick(buttonFive)
        }
        buttonFour.setOnClickListener {
            numberClick(buttonFour)
        }
        buttonThree.setOnClickListener {
            numberClick(buttonThree)
        }
        buttonTwo.setOnClickListener {
            numberClick(buttonTwo)
        }
        buttonOne.setOnClickListener {
            numberClick(buttonOne)
        }
        buttonDel.setOnClickListener {
            delete()
        }
        buttonDivide.setOnClickListener {
            addOperation(buttonDivide)
        }
        buttonMultiply.setOnClickListener {
            addOperation(buttonMultiply)
        }
        buttonMinus.setOnClickListener {
            addOperation(buttonMinus)
        }
        buttonPlus.setOnClickListener {
            addOperation(buttonPlus)
        }
        buttonDot.setOnClickListener {
            numberClick(buttonDot)
        }
    }

    private fun numberClick(button: Button) {
        numbers.text = numbers.text.toString() + button.text.toString()

    }

    private fun delete() {
        var text = numbers.text.toString()
        if (text.isNotEmpty()) {
            text = text.substring(0, text.length - 1)
            numbers.text = text
        } else {
            firstNumber = 0.0
            secondNumber = 0.0
            operation = ""
        }
    }

    private fun addOperation(button: Button) {

        if (numbers.text.isNotEmpty()) {
            operation = button.text.toString()
            firstNumber = numbers.text.toString().toDouble()
            numbers.text = ""
        }else if(numbers.text.toString()=="-" && firstNumber==0.0){
            numberClick(button)
        }
    }

    private fun equal() {
        if (numbers.text.isNotEmpty()) {
            secondNumber = numbers.text.toString().toDouble()
            var result: Double = 0.0
            if (operation == "+") {
                result = firstNumber + secondNumber
            } else if (operation == "-") {
                result = firstNumber - secondNumber
            } else if (operation == "*") {
                result = firstNumber * secondNumber
            } else if (operation == "/") {
                result = firstNumber / secondNumber
            }
            numbers.text = result.toString()
        }
}

}
